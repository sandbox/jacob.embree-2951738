<?php

/**
 * @file
 * Defines the view style plugins for views tablesorter module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_tablesorter_views_plugins() {
  return array(
    'style' => array(
      'tablesorter' => array(
        'title' => t('Tablesorter'),
        'help' => t('Display the results as a table using the tablesorter jQuery plugin.'),
        'handler' => 'views_tablesorter_plugin_style_tablesorter',
        'theme' => 'views_tablesorter_view_tablesorter',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'parent' => 'table',
      ),
    ),
  );
}

/**
 * Implements hook_views_pre_execute().
 */
function views_tablesorter_views_pre_execute(&$view) {
  if ($view->style_plugin->definition['handler'] == 'views_tablesorter_plugin_style_tablesorter') {
    // Disable the views pager before the query is executed as it interferes
    // with our pager. Keep the settings for our own pager though.
    if ($view->display_handler->options['pager']['type'] != 'none') {
      // @todo Setup the pager.
      //$view->display_handler->options['pager'] = $view->pager;
      //$view->pager['use_pager'] = 0;
      //$view->pager['items_per_page'] = 0;
    }
  }
}
